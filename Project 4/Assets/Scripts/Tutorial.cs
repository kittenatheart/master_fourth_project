﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial : MonoBehaviour
{
    Canvas tutorial;

    void Start()
    {
        tutorial = GetComponent<Canvas>();
        tutorial.enabled = true;
    }

    void Update()
    {
        if(Input.GetKeyUp(KeyCode.Tab))
        {
            tutorial.enabled = !tutorial.enabled;
        }
    }
}
