﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashLight : MonoBehaviour
{
    public static float batteryLife;
    public GameObject pov;
    public GameObject beam;
    Light flashLight;

    void Start()
    {
        flashLight = GetComponent<Light>();
        batteryLife = 10;
        pov.gameObject.SetActive(false);
        beam.gameObject.SetActive(false);
    }

    void Update()
    {
        if (batteryLife > 0f) //makes sure light has battery power
        {
            if (Input.GetKeyUp(KeyCode.E) && flashLight.enabled == false)
            { //if the light is off and the player toggles it, it turns on
                flashLight.enabled = true;
                pov.gameObject.SetActive(true);
                beam.gameObject.SetActive(true);
            }
            else if (Input.GetKeyUp(KeyCode.E))
            { //if the light is already on and the player toggles it, it turns off
                flashLight.enabled = false;
                pov.gameObject.SetActive(false);
                beam.gameObject.SetActive(false);
            }
        }
        else //if there is no battery power, the light turns off
        {
            flashLight.enabled = false;
            pov.gameObject.SetActive(false);
            beam.gameObject.SetActive(false);
        }
        if (flashLight.enabled == true)
            { //whenever the light is on, battery power decreases over time
                batteryLife -= 0.5f * Time.deltaTime;
                //use this line for testing purposes
                //Debug.Log(batteryLife);
        }
    }
}
/* keeping this in case I mess thigs up horriby
 * if (Input.GetKeyUp(KeyCode.E) && batteryLife > 0f)
        {
            flashLight.enabled = !flashLight.enabled;
        }
        */