﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightObserver : MonoBehaviour
{
    Transform enemy;

    bool m_IsEnemyInRange;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            m_IsEnemyInRange = true;
            enemy = other.transform;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            m_IsEnemyInRange = false;
        }
    }

    void Update()
    {
        if (m_IsEnemyInRange)
        {
            Vector3 direction = enemy.position - transform.position + Vector3.up;
            Ray ray = new Ray(transform.position, direction);
            RaycastHit raycastHit;
            if (Physics.Raycast(ray, out raycastHit))
            {
                if (raycastHit.collider.transform == enemy)
                {
                    enemy.gameObject.SetActive(false);
                }
            }
        }
    }
}
